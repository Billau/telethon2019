﻿Public Class Téléthon


    'Paramètres des variables
    Private donnation = 20.5
    Private saisie
    Private compte = 0
    'Paramètre à bouger en cas de bug
    Private kilometre = 50350
    Private gain = 1316.17
    'Pour le crono pour les h mettre celle restante et les minute vont de 59 à 0 ne pas toucher au seconde
    Private heure = 0
    Private minute = 0
    Private seconde = 0



    Private Sub NBsaisie_TextChanged(sender As Object, e As EventArgs) Handles NBsaisie.TextChanged
        saisie = NBsaisie.Text

    End Sub

    Private Sub ValiderSaisie_Click(sender As Object, e As EventArgs) Handles ValiderSaisie.Click

        gain = gain + saisie
        Me.GainTotal.Text = gain & " Euros"

    End Sub


    Private Sub DebutTimer_Click(sender As Object, e As EventArgs) Handles DebutTimer.Click

        Timer.Interval = 1000
        Timer.Enabled = True
        Timer.Start()

        DebutTimer.Visible = False
        FinTimer.Visible = True

    End Sub

    Private Sub Timer_Tick(sender As Object, e As EventArgs) Handles Timer.Tick

        Me.Text = "Téléthon " & Date.Now
        seconde -= 1

        If (seconde = 0) Then
            minute -= 1
            seconde = 60
        End If
        If (minute = 0) Then
            heure -= 1
            minute = 59
        End If

        Me.AfficheTimer.Text = heure & " H " & minute & " min " & seconde

    End Sub



    Private Sub FinTimer_Click(sender As Object, e As EventArgs) Handles FinTimer.Click

        Timer.Enabled = False
        Timer.Stop()

    End Sub

    Private Sub Téléthon_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Me.WindowState = FormWindowState.Maximized
        FinTimer.Visible = False


    End Sub

    Private Sub AfficheKilom_Click(sender As Object, e As EventArgs) Handles AfficheKilom.Click

        compte = compte + 1

        If (compte = 20) Then
            compte = 0
            gain = gain + donnation
            Me.GainTotal.Text = gain & " Euros"
        End If
        kilometre = kilometre + 50
        Me.AfficheKilom.Text = kilometre & " Mètres"

    End Sub

    Private Sub AfficheTimer_Click(sender As Object, e As EventArgs) Handles AfficheTimer.Click

        Timer.Interval = 1000
        Timer.Enabled = True
        Timer.Start()

    End Sub
End Class
