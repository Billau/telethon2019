﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Téléthon
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Téléthon))
        Me.ValiderSaisie = New System.Windows.Forms.Button()
        Me.GainTotal = New System.Windows.Forms.Label()
        Me.AfficheKilom = New System.Windows.Forms.Label()
        Me.DebutTimer = New System.Windows.Forms.Button()
        Me.NBsaisie = New System.Windows.Forms.TextBox()
        Me.Timer = New System.Windows.Forms.Timer(Me.components)
        Me.FinTimer = New System.Windows.Forms.Button()
        Me.AfficheTimer = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'ValiderSaisie
        '
        Me.ValiderSaisie.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.ValiderSaisie.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ValiderSaisie.Location = New System.Drawing.Point(255, 17)
        Me.ValiderSaisie.Name = "ValiderSaisie"
        Me.ValiderSaisie.Size = New System.Drawing.Size(135, 41)
        Me.ValiderSaisie.TabIndex = 0
        Me.ValiderSaisie.Text = "Valider"
        Me.ValiderSaisie.UseVisualStyleBackColor = True
        '
        'GainTotal
        '
        Me.GainTotal.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GainTotal.AutoSize = True
        Me.GainTotal.BackColor = System.Drawing.Color.Transparent
        Me.GainTotal.Font = New System.Drawing.Font("MV Boli", 79.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GainTotal.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.GainTotal.Location = New System.Drawing.Point(339, 706)
        Me.GainTotal.Name = "GainTotal"
        Me.GainTotal.Size = New System.Drawing.Size(832, 176)
        Me.GainTotal.TabIndex = 2
        Me.GainTotal.Text = "Gain obtenu"
        '
        'AfficheKilom
        '
        Me.AfficheKilom.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.AfficheKilom.AutoSize = True
        Me.AfficheKilom.BackColor = System.Drawing.Color.Transparent
        Me.AfficheKilom.Font = New System.Drawing.Font("MV Boli", 150.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AfficheKilom.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.AfficheKilom.Location = New System.Drawing.Point(60, 408)
        Me.AfficheKilom.Name = "AfficheKilom"
        Me.AfficheKilom.Size = New System.Drawing.Size(956, 327)
        Me.AfficheKilom.TabIndex = 3
        Me.AfficheKilom.Text = " 00 M"
        Me.AfficheKilom.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'DebutTimer
        '
        Me.DebutTimer.AutoSize = True
        Me.DebutTimer.FlatAppearance.BorderSize = 0
        Me.DebutTimer.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.DebutTimer.FlatAppearance.MouseOverBackColor = System.Drawing.Color.CornflowerBlue
        Me.DebutTimer.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DebutTimer.Location = New System.Drawing.Point(12, 91)
        Me.DebutTimer.Name = "DebutTimer"
        Me.DebutTimer.Size = New System.Drawing.Size(173, 54)
        Me.DebutTimer.TabIndex = 4
        Me.DebutTimer.Text = "Début"
        Me.DebutTimer.UseVisualStyleBackColor = False
        '
        'NBsaisie
        '
        Me.NBsaisie.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.NBsaisie.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NBsaisie.Location = New System.Drawing.Point(15, 17)
        Me.NBsaisie.Name = "NBsaisie"
        Me.NBsaisie.Size = New System.Drawing.Size(199, 41)
        Me.NBsaisie.TabIndex = 6
        '
        'Timer
        '
        '
        'FinTimer
        '
        Me.FinTimer.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FinTimer.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FinTimer.Location = New System.Drawing.Point(1206, 102)
        Me.FinTimer.Name = "FinTimer"
        Me.FinTimer.Size = New System.Drawing.Size(173, 52)
        Me.FinTimer.TabIndex = 7
        Me.FinTimer.Text = "Fini !"
        Me.FinTimer.UseVisualStyleBackColor = True
        '
        'AfficheTimer
        '
        Me.AfficheTimer.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.AfficheTimer.AutoSize = True
        Me.AfficheTimer.BackColor = System.Drawing.Color.Transparent
        Me.AfficheTimer.Font = New System.Drawing.Font("MV Boli", 48.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AfficheTimer.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.AfficheTimer.Location = New System.Drawing.Point(378, 265)
        Me.AfficheTimer.Name = "AfficheTimer"
        Me.AfficheTimer.Size = New System.Drawing.Size(728, 105)
        Me.AfficheTimer.TabIndex = 8
        Me.AfficheTimer.Text = "00 H 00 min 00"
        '
        'Panel1
        '
        Me.Panel1.AllowDrop = True
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1391, 172)
        Me.Panel1.TabIndex = 9
        '
        'Panel3
        '
        Me.Panel3.AllowDrop = True
        Me.Panel3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel3.BackColor = System.Drawing.Color.Transparent
        Me.Panel3.Controls.Add(Me.NBsaisie)
        Me.Panel3.Controls.Add(Me.ValiderSaisie)
        Me.Panel3.Location = New System.Drawing.Point(657, 59)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(449, 73)
        Me.Panel3.TabIndex = 11
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Transparent
        Me.Panel4.Controls.Add(Me.FinTimer)
        Me.Panel4.Controls.Add(Me.DebutTimer)
        Me.Panel4.Controls.Add(Me.Panel3)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel4.Location = New System.Drawing.Point(0, 898)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(1391, 157)
        Me.Panel4.TabIndex = 13
        '
        'Téléthon
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(1391, 1055)
        Me.Controls.Add(Me.GainTotal)
        Me.Controls.Add(Me.AfficheTimer)
        Me.Controls.Add(Me.AfficheKilom)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Panel1)
        Me.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Name = "Téléthon"
        Me.RightToLeftLayout = True
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.Text = "Téléthon"
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ValiderSaisie As Button
    Friend WithEvents GainTotal As Label
    Friend WithEvents AfficheKilom As Label
    Friend WithEvents DebutTimer As Button
    Friend WithEvents NBsaisie As TextBox
    Friend WithEvents Timer As Timer
    Friend WithEvents FinTimer As Button
    Friend WithEvents AfficheTimer As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Panel4 As Panel
End Class
